import math

def on_simulation_start(extension):
    make_trees_unmovable(extension)
    print("--- Starting Tree Physic ---")


def pre_step(extension):
    close_finger = extension.inputs.finger_position.value
    if close_finger == 0: # finger is closed
        make_trees_movable(extension)
    else:
        make_trees_unmovable(extension)

def make_trees_movable(extension):
    extension.outputs.control_type.value = 2

def make_trees_unmovable(extension):
    extension.outputs.control_type.value = 1
