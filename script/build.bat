pip install virtualenv 
virtualenv .env
cd ./py-operator-advisor
Set-ExecutionPolicy Unrestricted -Scope Process
.\.env\Scripts\activate.ps1
pip install maturin
maturin build --release
deactivate
pip install .\target\wheels\py_operator_advisor-0.1.0-cp38-none-win_amd64.whl --force-reinstall --user