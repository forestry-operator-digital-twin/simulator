import math
import py_operator_advisor

previous_data = None


def on_simulation_start(extension):
    print("--- Starting Observation logger ---")


def pre_step(extension):
    global previous_data
    daq_json_string = extension.inputs.daq.value
    if daq_json_string != "":
        if daq_json_string != previous_data:
            previous_data = daq_json_string
            observation = py_operator_advisor.convert_to_observation(
                daq_json_string)
            set_outputs(observation, extension)


def set_outputs(observation, extension):
    obs = [observation.tree_observation,
           observation.excavator_speed_observation,
           observation.excavator_angular_speed_observation,
           observation.stick_extension_observation,
           observation.stick_angle_observation,
           observation.finger_state_observation,
           ]
    extension.outputs.observations.value = str(obs)
