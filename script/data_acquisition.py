import json
from pathlib import Path
import time
from datetime import datetime

previous_time = time.time()
first_time = time.time()

step = 0
interval = 1
saving_path = r"C:\Users\betam3\Documents\operator_simulation\script\data\dataset-{}.json"

observations = {"data": [], "metadata": {"time_interval": interval, "unit_of_time": "s",
                                         "unit_of_distance": "m", "unit_of_angle": "rad", "starting_time_of_recording_unix": first_time*1000}}


def on_simulation_start(extension):
    global step
    global previous_time
    global first_time
    global observations
    step = 0
    previous_time = time.time()
    first_time = time.time()
    extension.outputs.daq.value = ""
    observations = {"data": [], "metadata": {"time_interval": interval, "unit_of_time": "s",
                                             "unit_of_distance": "m", "unit_of_angle": "rad", "starting_time_of_recording_unix": first_time*1000}}

    print("--- Starting Data Acquisition ---")


def pre_step(extension):
    global step
    global previous_time
    current_time = time.time()
    if current_time-previous_time > interval:
        print("step : {}".format(step))
        step_observation = {"step": step}
        collect_tree_observation(
            extension, step_observation)
        collect_excavator_observation(extension, step_observation)

        observations["data"].append(step_observation)
        export_results(observations, extension)

        previous_time = current_time
        step += 1


def export_results(res, extension):
    json_object = json.dumps(res)
    print(json_object)
    with open(saving_path.format(first_time), "w") as f:
        f.write(json_object)

    if (step+1) % 2 == 0 and step != 0:
        extension.outputs.daq.value = json.dumps(format_daq(res))


def format_daq(res):
    return {"data": res["data"][-2:], "metadata": res["metadata"]}


def convert_vortex_array_to_python_array(array):
    return [array[0], array[1], array[2]]


def collect_tree_observation(extension, observation):
    tree_locations = [
        (extension.inputs.tree_1.value, "tree_1"),
        (extension.inputs.tree_2.value, "tree_2"),
        (extension.inputs.tree_3.value, "tree_3"),
        (extension.inputs.tree_4.value, "tree_4"),
        (extension.inputs.tree_5.value, "tree_5"),
        (extension.inputs.tree_6.value, "tree_6"),
        (extension.inputs.tree_7.value, "tree_7"),
        (extension.inputs.tree_8.value, "tree_8"),
        (extension.inputs.tree_9.value, "tree_9"),
        (extension.inputs.tree_10.value, "tree_10"),
    ]
    for tree_position, tree_tag in tree_locations:
        observation[tree_tag] = convert_vortex_array_to_python_array(
            tree_position)


def collect_excavator_observation(extension, observation):
    excavator_observations = [
        (extension.inputs.pos_excavator.value, "position_excavator"),
        (extension.inputs.pos_stick.value, "position_stick"),
        (extension.inputs.pos_boom.value, "position_boom"),
        (extension.inputs.pos_hand.value, "position_hand"),
        (extension.inputs.pos_finger_1.value, "position_finger_1"),
        (extension.inputs.pos_finger_2.value, "position_finger_2"),
        (extension.inputs.speed_excavator.value, "speed_excavator"),
    ]

    for obs, tag in excavator_observations:
        observation[tag] = convert_vortex_array_to_python_array(obs)

    observation["angular_speed_chassis"] = extension.inputs.angular_speed_chassis.value
    observation["angular_pos_chassis"] = extension.inputs.angular_pos_chassis.value
