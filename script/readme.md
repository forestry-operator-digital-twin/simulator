# Script
Dynamic script for operator twin

## Build
`./build.bat --user`

the python interpreter must be set to the one of the [current environment and not the vortex studio default one](https://vortexstudio.atlassian.net/wiki/spaces/VSD21A/pages/2823137282/Integrating+Vortex+Studio+using+Python+3#IntegratingVortexStudiousingPython3-VortexStudioSDK-Python3Scripting-PythonUsageandEnvironmentConfiguration)

*path of the config file*

Options > Setup > Application Setup

![setupPath](https://gitlab.com/forestry-operator-digital-twin/simulator/-/raw/main/script/image/setup_path.JPG)


## Component

### Data Acquisition
`data_acquisition.py`

acquire raw data from the simulation

### Tree Physic
`tree_physic.py`

modify the physic dynamic of the trees of the simulation by making them unmovable when the prehension fingers is open and movable when the fingers are closed

### Observation Logger
`observation_logger.py`

send the transform the raw data into observation and log them into the labels of the simulation

## Support
Windows 10