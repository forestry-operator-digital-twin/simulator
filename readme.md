# Operator Twin
A prototype of a digital twin of a forest operator

# simulation

A [vortex studio](https://www.cm-labs.com/vortex-studio/) simulation that can be controlled with a game controller.

![scene](https://gitlab.com/forestry-operator-digital-twin/simulation/-/raw/main/image/scene.PNG)

# script

Python Modules to modify the behavior of the simulation and to make data acquisition. More detail in its own [readme.md](https://gitlab.com/forestry-operator-digital-twin/simulator/-/blob/main/script/readme.md) 